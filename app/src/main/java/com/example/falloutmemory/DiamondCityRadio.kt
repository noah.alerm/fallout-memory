package com.example.falloutmemory

class DiamondCityRadio {
    //METHODS
    /**
     * This method is used to access the full list of Diamond City Radio songs.
     * @return Array of Integers (e.g. R.raw.rocket_69)
     */
    fun getSongs() : Array<Int> {
        return arrayOf(R.raw.crawl_out_through_the_fallout, R.raw.atom_bomb_baby,
            R.raw.a_wonderful_guy, R.raw.accentuate_the_positive, R.raw.anything_goes,
            R.raw.butcher_pete_part_1, R.raw.butcher_pete_part_2, R.raw.civilization,
            R.raw.crazy_he_calls_me, R.raw.dear_hearts_and_gentle_people, R.raw.easy_living,
            R.raw.good_rocking_tonight, R.raw.grandma_plays_the_numbers, R.raw.happy_times,
            R.raw.he_s_a_demon_he_s_a_devil_he_s_a_doll,
            R.raw.i_don_t_want_to_set_the_world_on_fire, R.raw.into_each_life_some_rain_must_fall,
            R.raw.it_s_a_man, R.raw.it_s_all_over_but_the_crying, R.raw.keep_a_knockin,
            R.raw.maybe, R.raw.mighty_mighty_man, R.raw.one_more_tomorrow,
            R.raw.orange_colored_sky, R.raw.personality, R.raw.pistol_packin_mama,
            R.raw.right_behind_you_baby, R.raw.rocket_69, R.raw.sixty_minute_man,
            R.raw.the_end_of_the_world, R.raw.the_wanderer, R.raw.undecided, R.raw.uranium_rock,
            R.raw.uranium_fever, R.raw.way_back_home, R.raw.whole_lotta_shakin_goin_on,
            R.raw.worry_worry_worry)
    }
}
