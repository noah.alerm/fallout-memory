package com.example.falloutmemory

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider

class MenuActivity : AppCompatActivity() {
    //VARIABLES
    //layout
    private lateinit var difficultySpinner: Spinner
    private lateinit var playButton: Button
    private lateinit var helpButton: Button
    private lateinit var muteButton: ImageView

    //view model
    private lateinit var viewModel: MenuViewModel

    //ON CREATE
    @SuppressLint("SetTextI18n", "ResourceAsColor", "UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        //Launch Screen
        setTheme(R.style.SplashTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        //VIEW MODEL
        viewModel = ViewModelProvider(this).get(MenuViewModel::class.java)

        //IDs
        difficultySpinner = findViewById(R.id.difficulty_spinner)
        playButton = findViewById(R.id.play_button)
        helpButton = findViewById(R.id.help_button)
        muteButton = findViewById(R.id.mute)

        //INTRO AUDIO
        if (viewModel.isPlaying)
            viewModel.startIntro(this, muteButton)
        else
            viewModel.muteIntro(muteButton)

        //SPINNER SETUP
        val adapter = ArrayAdapter(this, R.layout.spinner_item,
            resources.getStringArray(R.array.difficulty))

        difficultySpinner.adapter = adapter

        //ON CLICK
        //play
        playButton.setOnClickListener {
            //Normal Mode
            if (difficultySpinner.selectedItem.toString() == "NORMAL") {
                val intent = Intent(this, GameNormalActivity::class.java)
                startActivity(intent)
            }
            //Survival Mode
            else if (difficultySpinner.selectedItem.toString() == "SURVIVAL") {
                val intent = Intent(this, GameSurvivalActivity::class.java)
                startActivity(intent)
            }
            //Deathclaw Mode
            else {
                val intent = Intent(this, GameDeathclawActivity::class.java)
                startActivity(intent)
            }
        }

        //help
        helpButton.setOnClickListener {
            //CUSTOM TITLE
            val title = TextView(this)
            title.text = "HELP"
            title.setTextSize(TypedValue.COMPLEX_UNIT_SP, 40F)
            title.gravity = Gravity.CENTER
            title.setTextColor(Color.parseColor("#FEF265"))
            title.typeface = Typeface.DEFAULT_BOLD
            title.background = getDrawable(R.drawable.help_dialog_title_bg)
            title.setPadding(0, 30, 0, 30)

            //CUSTOM DIALOG
            val builder = AlertDialog.Builder(this, R.style.HelpDialogTheme)
            val view = layoutInflater.inflate(R.layout.custom_help_dialog, null)
            builder.setView(view)
            builder.setCustomTitle(title)
            builder.setPositiveButton("OK", null)
            builder.setCancelable(false)
            val dialog = builder.create()
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT)) //Set to Transparent to only see the custom bg.
            dialog.show()
        }

        //intro
        muteButton.setOnClickListener {
            //Stops the intro if it's playing and starts it if it isn't.
            if (viewModel.mp.isPlaying) {
                viewModel.muteIntro(muteButton)
            } else {
                viewModel.startIntro(this, muteButton)
            }
        }
    }

    //INTRO CONTROLS
    override fun onPause() {
        super.onPause()
        viewModel.mp.pause()
    }

    override fun onResume() {
        super.onResume()
        viewModel.mp.start()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        //This is used to save the time of the intro when it's playing before changing the orientation.
        viewModel.introTime = viewModel.mp.currentPosition
    }
}
