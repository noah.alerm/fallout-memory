package com.example.falloutmemory

import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModel
import com.daimajia.androidanimations.library.Techniques
import com.daimajia.androidanimations.library.YoYo

class GameSurvivalViewModel : ViewModel() {
    //ATTRIBUTES
    //View Model
    var cards = mutableListOf<Card>()
    private var cardImages = arrayOf(R.drawable.adamantium_skeleton, R.drawable.four_leaf_clover,
    R.drawable.ghoulish, R.drawable.hacker, R.drawable.mister_sandman, R.drawable.sniper,
    R.drawable.adamantium_skeleton, R.drawable.four_leaf_clover, R.drawable.ghoulish,
    R.drawable.hacker, R.drawable.mister_sandman, R.drawable.sniper)

    //Activity
    //music
    private val songs = DiamondCityRadio().getSongs()
    lateinit var mp: MediaPlayer
    private var song = songs.indices.random()
    var isPlaying = false
    //moves
    var counter = 0

    //INIT
    init {
        setDataModel()
    }

    //FUNCTIONS
    /**
     * This method is used to initialize the game's data model.
     */
    private fun setDataModel() {
        cardImages.shuffle()
        for (i in 0..11)
            cards.add(Card(i, cardImages[i], flipped = false, matched = false))
    }

    //Music
    /**
     * This method is used to start the game's music.
     * @param context Game Context
     */
    fun startMusic(context: Context) {
        mp = MediaPlayer.create(context, songs[song])
        mp.start()
        isPlaying = true
    }

    /**
     * This method is used to restart the music after an orientation change.
     */
    fun restartMusic() {
        mp.seekTo(mp.currentPosition)
        mp.start()
    }

    //Card Management
    /**
     * This method is used to update a card's state.
     * @param cardID Card's ID on view model's list
     * @return Card's image (INT)
     */
    fun cardState(cardID: Int) : Int {
        return if (cards[cardID].flipped)
            cards[cardID].img
        else
            R.drawable.survival_card_back
    }

    /**
     * This method is used to flip a given card.
     * @param view Card's ImageView
     * @param cardID Card's ID on view model's list
     * @param moveCounter Move Counter TextView
     * @return card's new drawable
     */
    @SuppressLint("SetTextI18n")
    fun flipCard(view: ImageView, cardID: Int, moveCounter: TextView) : Int {
        //COUNTER UPDATE
        counter++
        moveCounter.text = "MOVEMENTS: $counter"

        //CARD FLIP
        if (!cards[cardID].matched)
            YoYo.with(Techniques.FlipInY).duration(300).playOn(view)
        cards[cardID].flipped = true

        //OUTPUT
        return cards[cardID].img
    }

    /**
     * This method is used to check for new matches.
     * @param views Card ImageView Array
     */
    fun checkIfMatch(views: Array<ImageView>) {
        var flippedCard1: Card? = null
        var flippedCard2: Card? = null

        //CHECK FOR NEW FLIPPED CARDS
        for (card in cards) {
            if (card.flipped && !card.matched) {
                if (flippedCard1 == null)
                    flippedCard1 = card
                else
                    flippedCard2 = card
            }
        }

        //NO MATCH
        if (flippedCard1 != null && flippedCard2 != null &&
            flippedCard1.img != flippedCard2.img) {
            //ANIMATION
            YoYo.with(Techniques.FlipInY).duration(300).playOn(views[flippedCard1.id])
            YoYo.with(Techniques.FlipInY).duration(300).playOn(views[flippedCard2.id])
            //FLIP BACK
            views[flippedCard1.id].setImageResource(R.drawable.survival_card_back)
            flippedCard1.flipped = false
            views[flippedCard2.id].setImageResource(R.drawable.survival_card_back)
            flippedCard2.flipped = false
        }
        //MATCH
        else {
            if (flippedCard1 != null && flippedCard2 != null) {
                flippedCard1.matched = true
                flippedCard2.matched = true

                //ANIMATION
                YoYo.with(Techniques.Tada).duration(300).playOn(views[flippedCard1.id])
                YoYo.with(Techniques.Tada).duration(300).playOn(views[flippedCard2.id])
            }
        }
    }
}
