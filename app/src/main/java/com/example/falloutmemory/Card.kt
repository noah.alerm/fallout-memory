package com.example.falloutmemory

/**
 * This class is used to store information about each card.
 * @param id Position in the ViewModel's list of cards.
 * @param img Card frontal image (R.drawable.img format)
 * @param flipped True if the card is flipped (Front view)
 * @param matched True if the card is matched (Cards with same img have been found)
 */
data class Card(var id: Int, var img: Int, var flipped: Boolean, var matched: Boolean)
